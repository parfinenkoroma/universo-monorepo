import { AUTO, Scale, Types } from 'phaser';

import SpinnerPlugin from 'phaser3-rex-plugins/templates/spinner/spinner-plugin.js';
import LoadingAnimationScenePlugin from 'phaser3-rex-plugins/plugins/loadinganimationscene-plugin.js';
import { COLOR_BACKGROUND } from '../kanbans/Const';

export const getPhaserConfig = (scene: any[]): Types.Core.GameConfig => ({
  type: AUTO,
  parent: 'phaser-container',
  width: window.innerWidth,
  height: window.innerHeight,
  scale: {
    mode: Phaser.Scale.NONE,
    autoCenter: Phaser.Scale.NO_CENTER,
    resizeInterval: 500,
    expandParent: true,
  },
  dom: {
    createContainer: true,
  },
  scene,
  autoFocus: true,
  disableContextMenu: true,
  input: {
    keyboard: {
      target: window,
    },
    touch: {
      target: null,
      capture: true,
    },
    windowEvents: true,
  },
  backgroundColor: COLOR_BACKGROUND,
  render: {
    roundPixels: true,
  },
  plugins: {
    // scene: [
    // {
    // key: 'rexUI',
    // plugin: RexPlugins,
    // mapping: 'rexUI',
    // },
    // // ...
    // ],
    global: [
      {
        key: 'rexLoadingAnimationScene',
        plugin: LoadingAnimationScenePlugin,
        start: true,
      },
    ],
    scene: [
      {
        key: 'rexSpinner',
        plugin: SpinnerPlugin,
        mapping: 'rexSpinner',
      },
    ],
  },
});
