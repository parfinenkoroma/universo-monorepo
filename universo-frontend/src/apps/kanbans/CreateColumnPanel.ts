import ObjectWrapper from 'src/utils/objectWrapper';
import {
  COLOR_LIGHT,
  COLOR_DARK,
  FONT_SMALL,
  COLOR_DRAG_STROKE,
  DefaultDepth,
  DragObjectDepth,
} from './Const';
import CreateItemsBox from '../cards/CreateItemsBox';
import { EndlessCanvas } from '../spaces-2d/EndlessCanvas';
import { EventBus, Events } from 'src/boot/eventBus';
import CreateDropDownList from './CreateDropDownList';
import { createModal } from './CreateModal';

const CreateColumnPanel = (scene: EndlessCanvas, column: ObjectWrapper) => {
  const panel = scene.rexUI.add
    .dialog({
      width: 120,
      space: { left: 10, right: 10, top: 10, bottom: 10 },
      //@ts-ignore

      background: scene.rexUI.add.roundRectangle({
        strokeColor: COLOR_DARK,
        radius: 0,
      }),
      title: CreateTitle(scene, column, () => {
        panel.layout();
      }),
      content: CreateItemsBox(scene, column.childrens, column.uuid),
      proportion: {
        content: 1,
      },
    })
    .setOrigin(0, 0);

  SetDraggable(panel);

  return panel;
};

const CreateTitle = (scene: EndlessCanvas, column: ObjectWrapper, callback) => {
  let textInput = '';
  const sizer = scene.rexUI.add
    .sizer({
      orientation: 'x',
      space: {
        left: 5,
        right: 0,
        top: 5,
        bottom: 5,
      },
    })
    .addBackground(
      scene.rexUI.add.roundRectangle(0, 0, 20, 20, 0, COLOR_LIGHT),
    );
  const innerText = scene.rexUI.add.label({
    text: scene.add.text(0, 0, column.name || '', {
      fontSize: FONT_SMALL,
      wordWrap: { width: 160 },
    }),
  });

  const dropDownOptions = [
    {
      label: 'Добавить карточку',
      id: 0,
      onClick: () => {
        createModal(scene, (text) => {
          textInput = text;
        }).then((button) => {
          scene.isInputMode = false;

          //@ts-ignore
          if (button.text === 'Сохранить' && textInput) {
            const payload = {
              nomo: textInput,
              //@ts-ignore
              kanvasoUuid: scene.store.getKanvaso[0].node.uuid,
              priskribo: textInput,
              tipoId: 4,
              ligiloPosedantoUuid: column.uuid,
            };

            //@ts-ignore
            scene.store.onEditKanvasoObjekto(payload);
          }
        });
      },
    },
    {
      label: 'Переименовать столбец',
      id: 1,
      onClick: () => {
        createModal(
          scene,
          (text) => {
            textInput = text;
          },
          //@ts-ignore
          scene.store.getCurrentState.get(column.uuid).name,
        ).then((data) => {
          scene.isInputMode = false;

          //@ts-ignore
          if (data.text === 'Сохранить' && textInput) {
            const payload = {
              nomo: textInput,
              uuid: column.uuid,
            };
            //@ts-ignore
            scene.store.onEditKanvasoObjekto(payload);
          }
        });
      },
    },
    {
      label: 'Удалить столбец',
      id: 2,
      onClick: () => {
        createModal(
          scene,
          null,
          null,
          'Удалить столбец',
          'Отменить',
          'Удалить',
        ).then((button) => {
          scene.isInputMode = false;

          //@ts-ignore
          if (button.text === 'Удалить') {
            const payload = {
              uuid: column.uuid,
              forigo: true,
            };
            //@ts-ignore
            scene.store.onEditKanvasoObjekto(payload);
          }
        });
      },
    },
  ];
  const dropDownButton = CreateDropDownList(scene, dropDownOptions);

  EventBus.$on(Events.Change, (uuid, object) => {
    if (uuid === column.uuid) {
      //@ts-ignore
      innerText.setText(scene.store.getCurrentState.get(uuid).name);
      sizer.getTopmostSizer().layout();
    }
  });
  sizer
    .add(innerText, { proportion: 1, expand: true })
    .add(dropDownButton, { proportion: 0, expand: true });
  sizer.layout();
  return sizer;
};

const SetDraggable = (panel: any) => {
  panel
    .setDraggable({
      sensor: 'title',
      target: panel,
    })
    .on('sizer.dragstart', OnPanelDragStart, panel)
    .on('sizer.dragend', OnPanelDragEnd, panel);
};

const OnPanelDragStart = function (this: any) {
  this.setDepth(DragObjectDepth);
  this.getElement('background').setStrokeStyle(3, COLOR_DRAG_STROKE);
};

const OnPanelDragEnd = function (this: any) {
  this.setDepth(DefaultDepth);
  this.getElement('background').setStrokeStyle(2, COLOR_DARK);
};

export default CreateColumnPanel;
