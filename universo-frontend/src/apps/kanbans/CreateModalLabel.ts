import { EndlessCanvas } from '../spaces-2d/EndlessCanvas';
import { COLOR_BLACK, COLOR_DRAG_STROKE, FONT_SMALL } from './Const';

const CreateModalLabel = (scene: EndlessCanvas, text: string) => {
  const normalBackground = scene.rexUI.add.roundRectangle(
    0,
    0,
    0,
    0,
    10,
    COLOR_BLACK,
    0.0,
  );
  normalBackground.setStrokeStyle(2, COLOR_BLACK); // Чёрная обводка

  const hoverBackground = scene.rexUI.add.roundRectangle(
    0,
    0,
    0,
    0,
    10,
    COLOR_BLACK,
    0.0,
  );
  hoverBackground.setStrokeStyle(2, COLOR_DRAG_STROKE);

  return scene.rexUI.add
    .label({
      background: normalBackground,
      text: scene.add.text(0, 0, text, {
        fontSize: FONT_SMALL,
        color: COLOR_BLACK.toString(), // Чёрный текст
      }),
      align: 'center',
      space: {
        left: 5,
        right: 5,
        top: 5,
        bottom: 5,
      },
    })
    .on('button.over', (button, groupName, index, pointer, event) => {
      button.getElement('background').destroy();
      button.setElement('background', hoverBackground);
    })
    .on('button.out', (button, groupName, index, pointer, event) => {
      button.getElement('background').destroy();
      button.setElement('background', normalBackground);
    });
};
export default CreateModalLabel;
