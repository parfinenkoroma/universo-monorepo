extends Object
# Здесь будем хранить всё для запросов к бэкэнду по блоку "rajtigo"


# URL к API (авторизация)
var URL_AUTH = null
# URL к API
var URL_DATA = null 


# func get_URL_AUTH():
# 	if not URL_AUTH:
# 		URL_AUTH = Net.http+"://"+Net.url_server+"/api/v1.1/registrado/"
# 	return URL_AUTH

func get_URL_AUTH():
	if not URL_AUTH:
		URL_AUTH = Net.http+"://localhost:8000/api/v1.1/"
	return URL_AUTH

# func get_URL_DATA():
# 	if not URL_DATA:
# 		URL_DATA = Net.http+"://"+Net.url_server+"/api/v1.1/"
# 	return URL_DATA

func get_URL_DATA():
	if not URL_DATA:
		URL_DATA = Net.http+"://localhost:8000/api/v1.1/"
	return URL_DATA

# Запрос авторизации
func auth_query(login, password):
	return JSON.stringify({ "query": "mutation { ensaluti(login: \"%s\", password: \"%s\") { status token message csrfToken uzanto { objId } } }" % [login, password] })


# Запрос никнейма
func get_nickname_query(id):
	return JSON.stringify({ "query": "query { profilo(siriusoUzantoId: %s) { edges { node { uuid retnomo } } } }" % id })


# запрос на актуальную версию программы
func get_aktuala_versio():
	var query = JSON.stringify(
		{ "query": "query { "+
		" universoAplikoVersio ( publikigo:true, tipo_Id:1, aktuala:true) " +
		" {edges{node{ tipo{ nomo{enhavo}} "+
		"  numeroVersio numeroSubversio numeroKorektado }} } }"} )
	# print('== get_aktuala_versio == ',query)
	return query
