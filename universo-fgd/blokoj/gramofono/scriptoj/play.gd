extends TextureButton

#при клике на play - она скрывается
#отображается disco
#включается музыка
func _on_play_pressed():
	get_node("/root/Title/gramofono/regpanelo/fono/play").set_visible(false)
	get_node("/root/Title/gramofono/regpanelo/fono/disco").set_visible(true)
	get_node("/root/Title/gramofono").stream_paused = false
