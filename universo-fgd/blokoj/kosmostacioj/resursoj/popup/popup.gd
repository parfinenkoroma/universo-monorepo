extends Control

@onready var konservejo_UI = $"/root/Title/CanvasLayer/UI/UI/konservejo"
@onready var konservejo_VBox = $"/root/Title/CanvasLayer/UI/UI/konservejo/VBox"
@onready var konservejo_Tree = $"/root/Title/CanvasLayer/UI/UI/konservejo/VBox/body_texture/HSplit/VBoxContainer/Tree"

const scene_transbapti = preload("res://blokoj/objektoj/scenoj/transbapti.tscn")


func _input(event):
	# проверяем, если открыто PopupMenu, а нажали в сторону
	if Input.is_action_just_pressed("left_click") or\
			Input.is_action_just_pressed("right_click"):
		var x = $".".get_global_mouse_position().x
		var y = $".".get_global_mouse_position().y
		if $canvas/PopupMenu.visible and not(($canvas/PopupMenu.offset_top<y) and \
					(y<($canvas/PopupMenu.offset_bottom+$canvas/PopupMenu.offset_top)) and \
					($canvas/PopupMenu.offset_left<x) and \
					(x<($canvas/PopupMenu.offset_right+$canvas/PopupMenu.offset_left))):
			$canvas/PopupMenu.visible=false
	# открываем PopupMenu
	if Input.is_action_just_pressed("right_click"):
		if event is InputEventMouseButton and konservejo_VBox.visible:
			$canvas/PopupMenu.set_item_disabled(2,true)
			var x = $".".get_global_mouse_position().x
			var y = $".".get_global_mouse_position().y
			if (konservejo_VBox.offset_top+konservejo_UI.global_position.y<y) and \
					(y<konservejo_VBox.offset_bottom+konservejo_UI.global_position.y) and \
					(konservejo_VBox.offset_left+konservejo_UI.global_position.x<x) and \
					(x<konservejo_VBox.offset_right+konservejo_UI.global_position.x):
				$canvas/PopupMenu.offset_left=x
				$canvas/PopupMenu.offset_top=y
				$canvas/PopupMenu.visible=true
				x = konservejo_Tree.get_local_mouse_position().x
				y = konservejo_Tree.get_local_mouse_position().y
				var index_pos = konservejo_Tree.get_item_at_position(Vector2(x,y))
#				print('==x= ',x,' y= ',y, ' item=',index_pos)
				index_pos.select(0)
				#если это имя корабля и корабль твой - переименовать
				var konservejo = index_pos.get_metadata(0)
				if konservejo: #выделен объект
					var modulo = konservejo['modulo']
					if modulo and modulo.get('resurso') and modulo['resurso'].get('tipo') and\
							modulo['resurso']['tipo'].get('objId') and\
							modulo['resurso']['tipo']['objId'] == 2:
						#разрешаем переименование корабля
						$canvas/PopupMenu.set_item_disabled(0,false)
					else:
						$canvas/PopupMenu.set_item_disabled(0,true)


const QueryObject = preload("res://kerno/menuo/skriptoj/queries.gd")



# выбрали позицию в меню
func _on_PopupMenu_index_pressed(index):
	var objekto = null
	if index == 0: # если выбран переименование
		var node_transbapti = scene_transbapti.instantiate()
		var select = konservejo_Tree.get_next_selected(null)
		if not select:
			return
		var konservejo = select.get_metadata(0)
		if konservejo.get('modulo'):
			node_transbapti.aldoni_objekto(konservejo['modulo'])
			Title.ui.add_child(node_transbapti,true)
			node_transbapti.popup_centered()

